package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class EndFragment extends Fragment {

    private OneViewModel mViewModel;
    private TextView score;

    public static EndFragment newInstance() {
        return new EndFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.end_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(OneViewModel.class);

        Game game = mViewModel.getGame();
        display(game);
    }

    private void display(Game game) {
        score.setText(game.getNumCorrectAnswers() + "");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        score = getView().findViewById(R.id.result);

        view.findViewById(R.id.play_again).setOnClickListener(this::RestartGame);
    }

    private void RestartGame(View view) {
        Navigation.findNavController(view).navigate(R.id.go_back_to_start);
    }
}
