package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;

public class QuestionFragment extends Fragment {

    //Variable declaration
    private OneViewModel mViewModel;
    private Button city1;
    private Button city2;
    private Button city3;
    private Button city4;
    private Button city5;
    private Button city6;
    private ImageView cityImg;

    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(OneViewModel.class);
        mViewModel.getGameLiveData().observe(this, this::onGameChanged);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cityImg = view.findViewById(R.id.imgCity);

        //Buttons
        city1 = view.findViewById(R.id.nCity1);
        city1.setTag(0);
        city1.setOnClickListener(this::changeFragmentQCity);

        city2 = view.findViewById(R.id.nCity2);
        city2.setTag(1);
        city2.setOnClickListener(this::changeFragmentQCity);

        city3 = view.findViewById(R.id.nCity3);
        city3.setTag(2);
        city3.setOnClickListener(this::changeFragmentQCity);

        city4 = view.findViewById(R.id.nCity4);
        city4.setTag(3);
        city4.setOnClickListener(this::changeFragmentQCity);

        city5 = view.findViewById(R.id.nCity5);
        city5.setTag(4);
        city5.setOnClickListener(this::changeFragmentQCity);

        city6 = view.findViewById(R.id.nCity6);
        city6.setTag(5);
        city6.setOnClickListener(this::changeFragmentQCity);

    }

    private void changeFragmentQCity(View view) {
        int number = (Integer) view.getTag();
        mViewModel.answerQuestion(number);
        Game game = mViewModel.getGame();
        onGameChanged(game);
    }

    private void onGameChanged(Game game){
        if(game.isFinished()){
            changeFragmentEnd();
        } else{
            display(game);
        }
    }

    private void changeFragmentEnd() {
        Navigation.findNavController(getView()).navigate(R.id.endFragment);
    }

    public void display(Game game){

        Question question =  game.getCurrentQuestion();
        List<City> cities = question.getPossibleCities();
        city1.setText(cities.get(0).getName());
        city2.setText(cities.get(1).getName());
        city3.setText(cities.get(2).getName());
        city4.setText(cities.get(3).getName());
        city5.setText(cities.get(4).getName());
        city6.setText(cities.get(5).getName());

        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        cityImg.setImageResource(resId);
    }

}
