package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class StartFragment extends Fragment implements Observer {

    private OneViewModel quizViewModel;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(OneViewModel.class);
        quizViewModel.getGameLiveData().observe(this, this::onChanged);
    }

    private void onChanged(Game game) {
    if (game != null){
            NavigateToQuiz(getView());
         }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.start_bttn).setOnClickListener(this::changeFragment);
    }

    private void changeFragment(View view) {
        quizViewModel.startQuiz();
        NavigateToQuiz(view);
    }

    private void NavigateToQuiz(View view){
        Navigation.findNavController(view).navigate(R.id.beginQuiz);
    }

    @Override
    public void onChanged(Object o) {

    }
}
