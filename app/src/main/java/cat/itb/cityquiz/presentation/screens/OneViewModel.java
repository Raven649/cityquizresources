package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class OneViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    Game game;
    MutableLiveData<Game> gameLiveData = new MutableLiveData<>();

    public MutableLiveData<Game> getGameLiveData() {
        return gameLiveData;
    }

    public void startQuiz() {
        game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
    }
    public Game getGame(){
        return game;
    }

    public void answerQuestion(int number) {
        Game game = gameLogic.answerQuestions(getGame() ,number);
    }




}
